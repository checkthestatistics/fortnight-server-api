const axios = require('axios');
const moment = require('moment');

const FortniteApi = require('../middleware/fortnite-api/stats');
const GetStatsDb = require('../middleware/get-stats-db');

const CURRENT_SEASON = 5;


exports.get_rankings = async (req, res, next) => {
  const { platform, mode, category, season, page } = req.params;
  const offset = (page - 1) * 100;

  await new Promise(done => setTimeout(done, 2 * 1000));

  const allowedCategories = ['score', 'kills', 'minutesplayed', 'matchesplayed', 'winratio', 'kdratio', 'fgscore', 'killspermatch', 'scorepermatch', 'top25', 'top12', 'top10', 'top6', 'top5', 'top3', 'top1'];
  if (!allowedCategories.some(allowedCat => allowedCat === category)) {
    return { msg: `There is no ranking for category ${category}`, status: 404 };
  }

  const allMode = mode === 'all';
  const escapedCat = res.db.escape(category).slice(1,-1);
  const categoryCol = 'a.'+ escapedCat;
  // const sumCategoryCol = allMode ? `SUM(${categoryCol}) ${escapedCat}` : categoryCol;
  // const matchesCol = allMode ? 'SUM(a.matchesplayed) matchesplayed' : 'a.matchesplayed';

  const rankingSql = `SELECT p.name player, ${categoryCol}, a.matchesplayed FROM st__alls a
    INNER JOIN st__all_platforms ap ON a.platform = ap.id
    INNER JOIN st__all_modes am ON a.mode = am.id
    INNER JOIN st__players p ON a.player_id = p.id
    WHERE ap.name = ? AND am.name = ? AND a.latest IS TRUE
    ORDER BY ${categoryCol} DESC
    LIMIT 100 OFFSET ?`;

  const recordsSql = `SELECT COUNT(${categoryCol}) records FROM st__alls a
    INNER JOIN st__all_platforms ap ON a.platform = ap.id
    INNER JOIN st__all_modes am ON a.mode = am.id
    WHERE ap.name = ? AND am.name = ? AND a.latest IS TRUE
    LIMIT 1`;

  const [[ranking], [[{ records }]]] = await Promise.all([
    res.db.execute(rankingSql, [platform, mode, offset]),
    res.db.execute(recordsSql, [platform, mode])
  ]);

  const roundedPages = parseInt(records / 100);

  return { 
    ...req.params,
    offset,
    records,
    pages: records / 100 - roundedPages > 0 ? roundedPages + 1 : roundedPages,
    ranking
  };
}

exports.get_stats = async (req, res, next) => {
  const { playerName, season } = req.params;
  const emptyStats = { 
    msg: `No stats were found for ${playerName}${season !== CURRENT_SEASON ? ' in season ' + season : '' }`,
    stats: [],
    status: 202
  };

  const [findPlayer] = await res.db.execute('SELECT id, name FROM st__players WHERE name = ? LIMIT 1', [playerName]);
  if (!findPlayer.length) return emptyStats;
  
  const dbStats = new GetStatsDb(res.db, season || CURRENT_SEASON, findPlayer[0].id);
  const stats = await dbStats.getEverything(findPlayer[0].name);
  if (!stats) return emptyStats;

  return { stats };
}

exports.get_players = async (req, res, next) => {
  const { playerName } = req.params;
  const sql = `SELECT p.name FROM st__players p
    INNER JOIN st__alls a ON  p.id = a.player_id
    WHERE p.name LIKE ? AND a.matchesplayed > 0
    GROUP BY p.name LIMIT 5`;

  const [getPlayers] = await res.db.execute(sql, [`%${playerName}%`]);
  return { players: getPlayers };
}

exports.get_last_records = async (req, res, next) => {
  const sql = `SELECT p.name, ap.name platform, am.name 'mode', a.score, a.kills, a.matchesplayed, p.updated_at updatedAt
    FROM st__alls a
    INNER JOIN st__players p ON a.player_id = p.id
    INNER JOIN st__all_platforms ap ON a.platform = ap.id
    INNER JOIN st__all_modes am ON a.mode = am.id
    GROUP BY a.player_id
    ORDER BY p.updated_at DESC
    LIMIT 7`;

  const [getLastRecords] = await res.db.execute(sql, [CURRENT_SEASON]);
  return { lastRecords: getLastRecords };
}

exports.patch_stats = async (req, res, next) => {
  const { playerName } = req.params;

  const fApi = new FortniteApi({
    email: process.env.FORTNITE_EMAIL,
    password: process.env.FORTNITE_PASSWORD,
    basicAuth1: process.env.FORTNITE_BASIC_AUTH_1,
    basicAuth2: process.env.FORTNITE_BASIC_AUTH_2,
    season: CURRENT_SEASON
  });
  await fApi.init(res.db);

  const sql = 'SELECT id, name, game_id gameId, updated_at updatedAt FROM st__players WHERE name = ? LIMIT 1';
  const [findPlayer] = await res.db.execute(sql, [playerName]);

  const verifyPlayer = async dbResult => {
    if (!dbResult.length) {
      const newPlayer = await fApi.findPlayer(playerName);
      if (!newPlayer) return;
      const { name, gameId } = newPlayer;
      const sql = 'INSERT INTO st__players (name, game_id) VALUES (?, ?)';
      const [insertPlayer] = await res.db.execute(sql, [name, gameId]);
      return { id: insertPlayer.insertId, name, gameId };
    } else {
      return {
        ...dbResult[0],
        updatedAt: moment(dbResult[0].updatedAt)
      }
    }
  }


  const verifiedPlayer = await verifyPlayer(findPlayer);
  if (!verifiedPlayer) return { stats: [], msg: `Player with nickname ${playerName} doesn't exist`, status: 202 };
  const dbStats = new GetStatsDb(res.db, CURRENT_SEASON, verifiedPlayer.id);

  const lastUpdate = moment().diff(verifiedPlayer.updatedAt, 'seconds');

  if (verifiedPlayer.updatedAt && lastUpdate < 170) {
    const stats = await dbStats.getEverything(verifiedPlayer.name);
    return { stats };
  }

  const allStats = await fApi.getStats(verifiedPlayer);
  if (!allStats.length) return { stats: [], msg: `Player with nickname ${playerName} doesn't have stats`, status: 202 };
  

  await fApi.calcAndSyncStats();

  const updateTime = findPlayer.length ? res.db.execute('UPDATE st__players SET updated_at = CURRENT_TIMESTAMP WHERE id = ?', [findPlayer[0].id]) : [];

  const [history, live] = await Promise.all([
    dbStats.history(),
    dbStats.live(),
    updateTime
  ]);

  return { 
    stats: { 
      general: dbStats.general(allStats, verifiedPlayer.name),
      all: allStats,
      history, 
      live 
    }
  }
}