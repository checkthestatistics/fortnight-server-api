exports.get_items = async (req, res, next) => {

	const sql = `SELECT w.id, n.name, wt.name 'type', r.name rarity, i.url img, wa.name ammoName, wa.img ammoImg, w.damage, w.dps, w.fire_rate fireRate, w.impact, w.headshot, w.magazine magSize
		FROM it__weapons w
		INNER JOIN it__names n ON w.name = n.id
		INNER JOIN it__weapons_types wt ON w.type = wt.id
		INNER JOIN it__rarity r ON w.rarity = r.id
		INNER JOIN it__images i ON w.img = i.id
		INNER JOIN it__weapons_ammo wa ON w.ammo = wa.id`;
	
	const [weapons] = await res.db.execute(sql);

	return { items: weapons.map(weapon => ({...weapon, category: 'weapon' })) };
}