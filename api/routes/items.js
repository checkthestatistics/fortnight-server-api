const express = require('express');
const router = express.Router();
const asyncHandler = require('../middleware/async-handler');

const itemsController = require('../controllers/items');

router.get('/', asyncHandler(itemsController.get_items));

module.exports = router;