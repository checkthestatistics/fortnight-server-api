const express = require('express');
const router = express.Router();
const asyncHandler = require('../middleware/async-handler');

const statsController = require('../controllers/stats');


router.get('/ranking/:platform/:mode/:category/season/:season/page/:page?', asyncHandler(statsController.get_rankings));

router.get('/players/:playerName', asyncHandler(statsController.get_players));

router.get('/last-records', asyncHandler(statsController.get_last_records));


router.get('/:playerName/:season?', asyncHandler(statsController.get_stats));

router.patch('/:playerName', asyncHandler(statsController.patch_stats));

module.exports = router;