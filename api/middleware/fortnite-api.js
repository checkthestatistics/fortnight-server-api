const axios = require('axios');
const cacheBase = require('cache-base');
const cache = new cacheBase(); 
const moment = require('moment');

const undefinedToZero = array => array.map(val => val == null ? 0 : val);

class FortniteApi {
  constructor () {
    this.accessToken = '';
    this.refreshToken = '';
    this.flattenedStats = [];
    this.allStats = [];
    this.season = 5;
    this.db;
    this.playerData;
  }

  async init(db) {
    this.db = db;
    const authData = cache.get();
    const timestamp = Date.now();

    if (!Object.keys(authData).length) {
      const newAuthData = await this.getAuthData();
      return newAuthData.accessToken;
    } 
    else if ((authData.expiresIn - timestamp) < 300 || (authData.refreshExpires - timestamp) < 300) {
      const newAuthData = await this.renewAuthData();
      return newAuthData.accessToken;
    } else {
      this.accessToken = authData.accessToken;
      this.refreshToken = authData.refreshToken;
      return authData.accessToken;
    }
  }

  async renewAuthData() {
    const url = 'https://account-public-service-prod03.ol.epicgames.com/account/api/oauth';
    const body = `grant_type=refresh_token&refresh_token=${this.refreshToken}&includePerms=true`;
    const renewAuthData = await axios.post(`${url}/token`, body, {
      headers: {
        'Authorization': process.env.FORTNITE_BASIC_AUTH_1,
        'Content-Type': 'multipart/form-data'
      }
    });

    const authData = {
      accessToken: renewAuthData.data.access_token,
      expiresIn: Date.now() + renewAuthData.data.expires_in * 1000,
      refreshToken: renewAuthData.data.refresh_token,
      refreshExpires: Date.now() + renewAuthData.data.refresh_expires * 1000
    }

    this.accessToken = authData.accessToken;
    this.refreshToken = authData.refreshToken;
    cache.set(authData);
    return authData;
  }

  async getAuthData() {
    const url = 'https://account-public-service-prod03.ol.epicgames.com/account/api/oauth';
    const initialTokenBody = `grant_type=password&username=${process.env.FORTNITE_EMAIL}&password=${process.env.FORTNITE_PASSWORD}&includePerms=true`;

    const initialToken = await axios.post(`${url}/token`, initialTokenBody, {
      headers: {
        'Authorization': process.env.FORTNITE_BASIC_AUTH_1
      }
    });

    const exchangeCode = await axios.get(`${url}/exchange`, {
      headers: {
        'Authorization': `bearer ${initialToken.data.access_token}`
      }
    });

    const authTokenBody = `grant_type=exchange_code&exchange_code=${exchangeCode.data.code}&includePerms=true&token_type=eg1`;

    const authRes = await axios.post(`${url}/token`, authTokenBody, {
      headers: {
        'Authorization': process.env.FORTNITE_BASIC_AUTH_2
      }
    });

    const authData = {
      accessToken: authRes.data.access_token,
      expiresIn: Date.now() + authRes.data.expires_in * 1000,
      refreshToken: authRes.data.refresh_token,
      refreshExpires: Date.now() + authRes.data.refresh_expires * 1000
    }
    console.log(this.refreshToken)
    this.accessToken = authData.accessToken;
    this.refreshToken = authData.refreshToken;
    cache.set(authData);
    return authData;
  }

  async findPlayer(playerName) {
    try {
      const url = `https://persona-public-service-prod06.ol.epicgames.com/persona/api/public/account/lookup?q=${encodeURI(playerName)}`;
      const player = await axios.get(url, {
        headers: {
          'Authorization': `bearer ${this.accessToken}`
        }
      });
      return player.data;
    } catch (err) {
    	// console.log(err)
      if (err.response.status==404) {
        // return [];
        return;
        throw new Error(`Player with name ${playerName} doesn't exist`);
      } else {
        console.log(err)
        throw new Error('Could not get player data from server, try again later');
      }
    }
  }

  async getStats(playerData) {
    this.playerData = playerData;
    const url = `https://fortnite-public-service-prod11.ol.epicgames.com/fortnite/api/stats/accountId/${playerData.gameId}/bulk/window/alltime`;
    const fetchStats = await axios.get(url, {
      headers: {
        'Authorization': `bearer ${this.accessToken}`
      }
    });
    if (!fetchStats.data.length) return null;

    const stats = fetchStats.data.reduce((prev, current) => {
      const [ , property, platform, , mode] = current.name.split('_');
      
      let modeName = '';
      switch(mode) {
        case 'p2':
          modeName = 'duo';
          break;
        case 'p9':
          modeName = 'squad';
          break;
        case 'p10':
          modeName = 'solo';
          break;
      }

      const propName = property.replace(/place/g,'');
      
      prev[platform] = prev[platform] || {};
      prev[platform][modeName] = prev[platform][modeName] || {};
     
      prev[platform][modeName][propName] = current.value;
      return prev;
    }, {});

    for (let platform in stats) {
      for (let mode in stats[platform]) {
        const props = ['matchesplayed', 'score', 'kills', 'minutesplayed', 'top25', 'top12', 'top10', 'top6', 'top5', 'top3', 'top1', 'lastmodified'];
        props.forEach(prop => stats[platform][mode][prop] = stats[platform][mode][prop] || 0);
      }
    }

    const flatStats = Object.keys(stats).reduce((arr, platform) => 
      [
        ...arr, 
        ...Object.keys(stats[platform]).map(mode => {
          const props = ['matchesplayed', 'score', 'kills', 'minutesplayed', 'top25', 'top12', 'top10', 'top6', 'top5', 'top3', 'top1', 'lastmodified'];
          return {
            platform,
            mode,
            ...stats[platform][mode],
            ...props.reduce((obj, prop) => ({...obj, [prop]: stats[platform][mode][prop] || 0 }), {})
          }
        })
      ], []);

    this.flattenedStats = flatStats;
    this.allStats = stats;
    return [ stats, flatStats ];
  }

  async getGeneralStats() {
    const generalStats = this.flattenedStats.reduce((reduced, stats) => {
      reduced.matchesplayed += stats.matchesplayed || 0;
      reduced.kills += stats.kills || 0;
      reduced.score += stats.score || 0;
      reduced.minutesplayed += stats.minutesplayed || 0;
      reduced.wins += stats.top1 || 0;
      return reduced;
    }, { matchesplayed: 0, kills: 0, score: 0, minutesplayed: 0, wins: 0 });
  
    const roundNum = (number, scale = 10) => Math.round(number * scale) / scale;
  
    generalStats.winratio = roundNum((generalStats.wins / generalStats.matchesplayed) * 100) || 0;
    generalStats.kdratio = roundNum(generalStats.kills / (generalStats.matchesplayed - generalStats.wins)) || 0;
    generalStats.hoursplayed = parseInt(generalStats.minutesplayed / 60);
    generalStats.name = this.playerData.name;
    const gS = generalStats;

    const updateSql = `UPDATE st__general 
      SET matchesplayed= ?, kills= ?, score= ?, minutesplayed= ?, kdratio= ?, winratio= ?, wins= ?, updated_at = CURRENT_TIMESTAMP
      WHERE st__general.player_id = ? AND season = ?`;
    
    const insertSql = `INSERT INTO st__general
      (matchesplayed, kills, score, minutesplayed, kdratio, winratio, wins, player_id, season)
      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`;

    const sqlVals = undefinedToZero([gS.matchesplayed, gS.kills, gS.score, gS.minutesplayed, gS.kdratio, gS.winratio, gS.wins, this.playerData.id, this.season]);
    
    const [updateStats] = await this.db.execute(updateSql, sqlVals);
    // console.log(updateStats)
    if (updateStats.affectedRows == 0) {
      await this.db.execute(insertSql, sqlVals);
    }
    this.generalStats = generalStats;
    return {...generalStats, season: this.season };
  }

  async getAllStats() {
    const updateSql = `UPDATE st__all a
      INNER JOIN st__all_platforms ap ON a.platform=ap.id
      INNER JOIN st__all_modes am ON a.mode=am.id
      SET matchesplayed= ?, score= ?, kills= ?, minutesplayed= ?, lastmodified= ?, top25= ?, top12= ?, top10= ?, top6= ?, top5= ?, top3= ?, top1= ?
      WHERE a.player_id= ? AND a.season= ? AND ap.name= ? AND am.name= ?`;

    const insertSql = `INSERT INTO st__all
      (platform, mode, matchesplayed, score, kills, minutesplayed, lastmodified, top25, top12, top10, top6, top5, top3, top1, player_id, season) 
      SELECT ap.id, am.id, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
      FROM st__all_platforms ap, st__all_modes am
      WHERE ap.name= ? AND am.name= ?`;

    const sqlVals = this.flattenedStats.map(s => [s.matchesplayed, s.score, s.kills, s.minutesplayed, s.lastmodified, s.top25, s.top12, s.top10, s.top6, s.top5, s.top3, s.top1, this.playerData.id, this.season, s.platform, s.mode]);
    
    const updatePromises = sqlVals.map(valsArr => this.db.execute(updateSql, undefinedToZero(valsArr)));
    const updateStats = await Promise.all(updatePromises);
  
    const insertPromises = updateStats
      .reduce((notUpdatedIndexes, updateArr, index) => (updateArr[0].affectedRows == 0) ? [...notUpdatedIndexes, index] : notUpdatedIndexes, [])
      .map(insertIndex => this.db.execute(insertSql, undefinedToZero(sqlVals[insertIndex])));
    await Promise.all(insertPromises);

    return this.flattenedStats;
  }

  async getStatsHistory () {
    const todaysDate = moment().format('YYYY-MM-DD');

    const prevRecordsSql = `SELECT * FROM st__history WHERE player_id = ? AND season = ? ORDER BY date DESC LIMIT 11`;
    const [getPrevRecords] = await this.db.execute(prevRecordsSql, [this.playerData.id, this.season]);
    const prevRecords = getPrevRecords.map(record => ({
        ...record,
        date: moment(record.date, 'YYYY-MM-DD').format('YYYY-MM-DD')
      })
    );
    const lastRecordDate = prevRecords[0] ? prevRecords[0].date : null;

    const calcDiffs = array => array.map((record, index) => {
      const nextIndex = index+1 == array.length ? index : index+1;
      const calcPropDiff = prop => record[prop] - array[nextIndex][prop];
      return ['score', 'matchesplayed', 'kills']
        .reduce((obj, prop) => ({ ...obj, [prop]: calcPropDiff(prop) }), { date: record.date });
    });     

    const insertSql = `INSERT INTO st__history (score, matchesplayed, kills, player_id, date, season) VALUES (?, ?, ?, ?, ?, ?)`;
    const updateSql = `UPDATE st__history SET score = ?, matchesplayed = ?, kills = ? WHERE player_id = ? AND date = ? AND season = ?`;
    const { score, matchesplayed, kills } = this.generalStats;
    const sqlVals = [score, matchesplayed, kills, this.playerData.id, todaysDate, this.season];
    const valsObj = {score, matchesplayed, kills, date: todaysDate};

    if (lastRecordDate == todaysDate) {
      await this.db.execute(updateSql, sqlVals);
      prevRecords.splice(0,1);
      const res = calcDiffs([valsObj, ...prevRecords]);
      res.splice(res.length-1,1);
      return res;
    } else {
      await this.db.execute(insertSql, sqlVals);
      const res = calcDiffs([valsObj, ...prevRecords]);
      res.splice(res.length-1, 1);
      return res;
    }
  }
}

module.exports = FortniteApi;