module.exports = fn => (request, response, next) => new Promise((resolve) => {
	resolve(fn(request, response, next));
})
.then(res => {
	response.db.end();
	const status = res.status || 200;
	const result = Object.assign(res, { status: undefined });
	return response.status(status).json(result);
})
.catch(err => {
	response.db.end();
	if (err instanceof Error) {
		next(err);
	} else {
		return response.status(err.status || 500).json(err);
	}
});