const DbActions = require('./db-actions');
const moment = require('moment');


class Stats extends DbActions {
  
  constructor(...args) {
    super(...args);

    this.fetchedStats = this.baseStats;
    this.props = ['matchesplayed', 'score', 'kills', 'minutesplayed', 'winratio', 'kdratio', 'fgscore', 'killspermatch', 'scorepermatch', 'top25', 'top12', 'top10', 'top6', 'top5', 'top3', 'top1'];
      
  }

  async calcAndSyncStats() {
    this.storedStats = await this.getAllStats();
    const [statsToUpdate, statsToInsert, liveStatsToCalc, outdatedStatsIndexes] = this.groupAndCompareStats();
    const liveStatsToInsert = liveStatsToCalc.length ? this.calcLiveStats(liveStatsToCalc) : [];

    const statsToDbProceed = [
      { data: statsToUpdate, fnName: 'updateAllStats' }, 
      { data: statsToInsert, fnName: 'insertAllStats' },
      { data: liveStatsToInsert, fnName: 'insertLiveStats' },
      { data: outdatedStatsIndexes, fnName: 'updateOutdatedStatsStatus' }
    ].filter(stats => stats.data.length);

    const transactionSql = statsToDbProceed.reduce((sql, statsToProceed) => {
      return sql += this[statsToProceed.fnName](statsToProceed.data)
    }, 'START TRANSACTION; ');

    await this.db.query(transactionSql + ' COMMIT;');

    return liveStatsToInsert;
  }

  groupAndCompareStats() {

      const actualDate = moment().utc().format('YYYY-MM-DD') + ' 00:00:00';
      this.fetchedStats = this.baseStats;

      return this.fetchedStats.reduce((grouped, current) => {

        const existingRecordFromDb = this.storedStats.find(record => record.platform === current.platform && record.mode === current.mode);
        const recordIsFromToday = existingRecordFromDb ? moment(existingRecordFromDb.updatedAt).isSameOrAfter(actualDate) : false;

        const groupTo = (groupName, propsToUpdate = []) =>
          groupName === 'update' 
          ? [[...grouped[0], { id: existingRecordFromDb.id, ...current, propsToUpdate }], grouped[1], grouped[2], grouped[3]]
          : [grouped[0], [...grouped[1], current], grouped[2], grouped[3]];

        const getPropsToUpdate = () => this.props.reduce((propsToUpdate, prop) => 
          existingRecordFromDb ? (current[prop] - existingRecordFromDb[prop] === 0 ? propsToUpdate : [...propsToUpdate, prop]) : propsToUpdate
        , []);

        const propsToUpdate = getPropsToUpdate();
        const dataHasChanged = !!propsToUpdate.length;

        console.log('dataHasChanged', dataHasChanged)
        // console.log(recordIsFromToday);

        if (!existingRecordFromDb) {
          grouped = groupTo('insert');
        } else if (dataHasChanged && recordIsFromToday) {
          grouped = groupTo('update', propsToUpdate);
        } else if (dataHasChanged && recordIsFromToday === false) {
          grouped[3] = [...grouped[3], existingRecordFromDb.id];
          grouped = groupTo('insert');
        }

        if (existingRecordFromDb && dataHasChanged) {
          grouped[2] = [...grouped[2], current];
        }

        return grouped;
      },[[], [], [], []]);
  }

  calcLiveStats(liveStatsToCalc) {

    const propsToCalc = ['score', 'fgscore', 'matchesplayed', 'kills', 'minutesplayed', 'top25', 'top12', 'top10', 'top6', 'top5', 'top3', 'top1'];

    return liveStatsToCalc.reduce((liveStats, stats) => {
      const { platform, mode } = stats;
      const dbRecord = this.storedStats.find(record => record.platform === stats.platform && record.mode === stats.mode);

      const statsDiff = propsToCalc.reduce((diffs, prop) => 
        ({ ...diffs, [prop]: stats[prop] - dbRecord[prop] })
      , {});

      const parsedStats = {
        ...statsDiff,
        top10: statsDiff.top3 + statsDiff.top5 + statsDiff.top6 + statsDiff.top10,
        top25: statsDiff.top12 + statsDiff.top12
      }

      const propsToDel = ['top3', 'top5', 'top6', 'top12'];
      propsToDel.forEach(prop => { delete parsedStats[prop] });

      if (!Object.values(parsedStats).some(prop => prop !== 0)) return liveStats;

      return [
        ...liveStats,
        { platform, mode, ...parsedStats }
      ];
    }, []);
  }


  async statsHistory2() {

    const sql = `SELECT * FROM st__history WHERE player_id = ? AND season = ? ORDER BY date DESC LIMIT 11`;
    const [previousStats] = await this.db.execute(sql, [this.player.id, this.season]);
    if (!previousStats.length < 2) return [];

    const propsToCalc = ['matchesplayed', 'score', 'kills'];

    const statsHistory = stats.slice(0, -1)
      .map((current, index) => ({
        ...current,
        ...propsToCalc.reduce((calced, prop) => ({...calced, [prop]: current[prop] - stats[index+1][prop] }), {})
      }));

  }

  async statsHistory () {
    const sql = `SELECT a.id, ap.name platform, am.name mode, a.matchesplayed, a.score, a.minutesplayed, a.kills, a.top1, a.top10, a.top25, a.updated_at updatedAt
      FROM st__alls a 
      INNER JOIN st__all_platforms ap ON a.platform = ap.id
      INNER JOIN st__all_modes am ON a.mode = am.id
      WHERE a.player_id = ? AND a.season = ?
      GROUP BY platform, mode 
      ORDER BY updated_at DESC`;

    const [savedStats] = await this.db.execute(sql, [this.player.id, this.season]);
    // console.log(savedStats)
    const reduced = savedStats.reduce((reduced, savedRecord) => {
      const updatedRecord = this.baseStats.find(record => record.platform === savedRecord.platform && record.mode === savedRecord.mode);

      console.log(savedRecord.matchesplayed)
      console.log(updatedRecord.matchesplayed)

      const matchesplayed = updatedRecord.matchesplayed - savedRecord.matchesplayed;
      if (matchesplayed > 0) reduced.push({
        platform: updatedRecord.platform,
        mode: updatedRecord.mode,
        matchesplayed,
        date: moment().utc()
      });

      return reduced;
    }, []);
    return reduced;
    console.log(reduced)
    // console.log(savedStats)
      
  }
}

module.exports = Stats;