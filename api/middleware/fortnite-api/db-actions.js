const FortniteApi = require('./api');
const moment = require('moment');

class DbActions extends FortniteApi {

  constructor(...args) {
    super(...args);
  }

  async getAllStats() {
    const propsToSelect = this.props.reduce((sql, prop) => sql += ` a.${prop},`, '');
     
    const sql = `SELECT a.id, ap.name platform, am.name 'mode', ${propsToSelect} a.updated_at updatedAt, a.latest
      FROM st__alls a 
      INNER JOIN st__all_platforms ap ON a.platform = ap.id
      INNER JOIN st__all_modes am ON a.mode = am.id
      WHERE a.player_id = ? AND a.season = ? AND a.latest IS TRUE
      GROUP BY a.mode
      LIMIT 9`;
    
    const [storedStats] = await this.db.execute(sql, [this.player.id, this.season]);
    this.storedStats = storedStats;
    return storedStats;
  }

  insertAllStats(allStats) {
    return allStats.reduce((sql, stats) => {
      const propsToSet = this.props.reduce((sql, prop) => sql += prop + ', ' , '');
      const valsToSet = this.props.reduce((sql, prop) => 
        sql += `${this.db.escape(stats[prop])}, ` , ``) + this.db.escape(this.player.id) + ', ' + this.db.escape(this.season);

      return sql += `INSERT INTO st__alls
        (platform, mode, ${propsToSet}player_id, season) 
        SELECT ap.id, am.id, ${valsToSet}
        FROM st__all_platforms ap, st__all_modes am
        WHERE ap.name = ${this.db.escape(stats.platform)} AND am.name = ${this.db.escape(stats.mode)}; `;
    }, ``);
  }

  updateAllStats(allStats) {
    return allStats.reduce((sql, stats) => {
      const propsWithVals = stats.propsToUpdate.reduce((sql, prop, index) => 
        sql += `${prop} = ${this.db.escape(stats[prop])}${index === stats.propsToUpdate.length - 1 ? ' ' : ', '}`
      , '');
      return sql += `UPDATE st__alls a 
        INNER JOIN st__all_platforms ap ON a.platform = ap.id
        INNER JOIN st__all_modes am ON a.mode = am.id
        SET ${propsWithVals}
        WHERE a.id = ${this.db.escape(stats.id)}; `;
    }, '');
  }

  insertLiveStats(liveStats) {
    const actualDate = moment().utc().format('YYYY-MM-DD') + ' 00:00:00';
    const escape = value => this.db.escape(value);
    
    return liveStats.reduce((sql, stats) => {
      const { platform, mode } = stats;
      const insertVals = Object.values(stats).slice(2).reduce((sql, val) =>
        sql += this.db.escape(val) + ' , '
      ,'');
      

      return sql += `INSERT INTO st__live
        (platform, mode, score, fgscore, matchesplayed, kills, minutesplayed, top25, top10, top1, player_id)
        SELECT ap.id, am.id, ${insertVals}${escape(this.player.id)}
        FROM st__all_platforms ap, st__all_modes am
        WHERE ap.name = ${escape(platform)} AND am.name = ${escape(mode)} AND NOT EXISTS (
          SELECT null FROM st__live l
          INNER JOIN st__all_platforms ap ON l.platform = ap.id
          INNER JOIN st__all_modes am ON l.mode = am.id
          WHERE l.platform = ${escape(stats.platform)} AND l.mode = ${escape(stats.mode)} AND l.player_id = ${escape(this.player.id)}
          AND l.score = ${escape(stats.score)} AND l.date >= '${actualDate}'
        ); `;
    }, '');
  }

  updateOutdatedStatsStatus(recordsIndexes) {
    return recordsIndexes.reduce((sql, index) => 
      sql += `UPDATE st__alls SET latest = 0, updated_at = updated_at WHERE st__alls.id = ${this.db.escape(index)}; `
    ,'');
  }

}

module.exports = DbActions;