const axios = require('axios');
const cacheBase = require('cache-base');
const cache = new cacheBase();

class FortniteApi {

  constructor(config) {

    if (!Object.keys(config).length) {
      throw new Error('Please provide configuration file with Fortnite login details');
    }

    this.config = config;
    this.season = config.season;
    this.accessToken = '';
    this.refreshToken = '';
    this.player = null;
    this.baseStats = [];
  }

  async init(db) {
    this.db = db;
    const authData = cache.get();
    const timestamp = Date.now();
    
    if (!Object.keys(authData).length) {
      await this.getAuthData(); 
    } else if (authData.expiresIn - timestamp < 600) {
      await this.renewAuthData();
    } else {
      this.accessToken = authData.accessToken;
      this.refreshToken = authData.refreshToken;

    }
  }

  prepAuthHeader(authValue) {
    return ({ headers: { 'Authorization': authValue } });
  }

  async getAuthData() {
    try {

      const baseUrl = 'https://account-public-service-prod03.ol.epicgames.com/account/api/oauth';

      const initialTokenBody = `grant_type=password&username=${this.config.email}&password=${this.config.password}&includePerms=true`;
      const initialToken = await axios.post(`${baseUrl}/token`, initialTokenBody, this.prepAuthHeader(this.config.basicAuth1));

      const exchangeCode = await axios.get(`${baseUrl}/exchange`, this.prepAuthHeader(`bearer ${initialToken.data.access_token}`));

      const authTokenBody = `grant_type=exchange_code&exchange_code=${exchangeCode.data.code}&includePerms=true&token_type=eg1`;
      const authRes = await axios.post(`${baseUrl}/token`, authTokenBody, this.prepAuthHeader(this.config.basicAuth2));

      this.setAuthData(authRes);

    } catch (err) {
      throw err;
    }
  }

  async renewAuthData() {
    try {

      const url = 'https://account-public-service-prod03.ol.epicgames.com/account/api/oauth';
      const body = `grant_type=refresh_token&refresh_token=${this.refreshToken}&includePerms=true`;
      const renewedAuthData = await axios.post(`${url}/token`, body, this.prepAuthHeader(this.config.basicAuth2));
      this.setAuthData(renewedAuthData);

    } catch(err) {
      throw err;
    }
  }

  setAuthData(newData) {
    const authData = {
      accessToken: newData.data.access_token,
      expiresIn: Date.now() + newData.data.expires_in * 1000,
      refreshToken: newData.data.refresh_token,
      refreshExpires: Date.now() + newData.data.refresh_expires * 1000
    }
    this.accessToken = authData.accessToken;
    this.refreshToken = authData.refreshToken;
    cache.set(authData);
    return authData;
  }

  async findPlayer(playerName) {
    try {

      if (!playerName.length) throw new Error('Please provide player name');

      const url = `https://persona-public-service-prod06.ol.epicgames.com/persona/api/public/account/lookup?q=${playerName}`;
      const encodedUrl = (encodeURI(url)).trim();
      const foundPlayer = await axios.get(encodedUrl, this.prepAuthHeader(`Bearer ${this.accessToken}`));

      const playerData = {
        gameId: foundPlayer.data.id,
        name: foundPlayer.data.displayName
      }

      return playerData;

    } catch (err) {

      if (err.response && err.response.status === 404) {
        return;
      } else {
        throw err;
      }

    }
  }

  async getStats(playerData) {
    try {
      
      if (!playerData && !playerData.id.length) {
        throw new Error(`Can't get stats if you haven't provided player's game id. Alternatively run function 'findPlayer(playerName)'`);
      }  

      this.player = playerData;

      const url = `https://fortnite-public-service-prod11.ol.epicgames.com/fortnite/api/stats/accountId/${this.player.gameId}/bulk/window/alltime`;
      const fetchStats = await axios.get(url, this.prepAuthHeader(`bearer ${this.accessToken}`));
      if (!fetchStats.data.length) return [];
 
      const normalizedStats = fetchStats.data.reduce((prev, current) => {
        const [ , property, platform, , mode] = current.name.split('_');
        let modeName = '';
        switch(mode) {
          case 'p2':
            modeName = 'solo';
            break;
          case 'p9':
            modeName = 'squad';
            break;
          case 'p10':
            modeName = 'duo';
            break;
        }
      
        const propName = property.replace(/place/g,'');
        const existingRecordIndex = prev.findIndex(record => record.platform == platform && record.mode == modeName);
      
        if (existingRecordIndex > -1) {
          prev[existingRecordIndex][propName] = current.value;
          return prev;
        } else {
          return [...prev, {
            platform,
            mode: modeName,
            [propName]: current.value
          }]
        }
      }, []);

      const roundNum = number => Math.round(number * 100) / 100;
      const baseStatsProps = ['matchesplayed', 'score', 'kills', 'minutesplayed', 'top25', 'top12', 'top10', 'top6', 'top5', 'top3', 'top1', 'lastmodified'];
      
      const extendedStats = normalizedStats
        .map(record => {
          for (const prop of baseStatsProps) {
            if (!record[prop]) record[prop] = 0;
          }
          return record
        })
        .map(record => ({
          ...record,
          fgscore: 0,
          winratio: roundNum(record.top1 / record.matchesplayed * 100),
          kdratio: roundNum(record.kills / (record.matchesplayed - record.top1)),
          killspermatch: roundNum(record.kills / record.matchesplayed),
          scorepermatch: roundNum(record.score / record.matchesplayed)
        }))
        .sort((a, b) =>
          (a.mode == 'duo' && b.mode == 'solo') || (a.mode == 'squad' && b.mode != 'squad') ? 1 : 0
        )
        .sort((a, b) => {
          if (a.platform == b.platform) return 0;
          return a.platform < b.platform ? -1 : 1;
        });

        this.availablePlatforms = extendedStats
          .reduce((platforms, record) => platforms.some(platform => platform === record.platform) ? platforms : [...platforms, record.platform], []);

        this.baseStats = extendedStats;
        return extendedStats;

    } catch (err) {
      throw err;
    }
  }
}

module.exports = FortniteApi;