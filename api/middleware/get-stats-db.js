
class GetStatsDb {

  constructor(db, season, playerId) {
    this.db = db;
    this.season = season;
    this.playerId = playerId;
  }

  async getEverything(playerName) {
    const [all, history, live] = await Promise.all([
      this.all(),
      this.history(),
      this.live()
    ]);
    if (!all.length) return;
    const general = this.general(all, playerName);

    return {
      general,
      all,
      history,
      live
    }
  }

  async all() {
    const sql = `SELECT a.id, ap.name platform, am.name 'mode', a.matchesplayed, a.score, a.kills, a.minutesplayed, a.winratio, a.kdratio, a.fgscore, a.killspermatch, a.scorepermatch, a.top25, a.top12, a.top10, a.top6, a.top5, a.top3, a.top1, MAX(a.updated_at) updatedAt
      FROM st__alls a 
      INNER JOIN st__all_platforms ap ON a.platform = ap.id
      INNER JOIN st__all_modes am ON a.mode = am.id
      WHERE a.player_id = ? AND a.season = ?
      GROUP BY platform, mode 
      LIMIT 9`;
    const [allStats] = await this.db.execute(sql, [this.playerId, this.season]);
    return allStats;
  }

  async live() {
    const sql = `SELECT ap.name platform, am.name 'mode', l.score, l.fgscore, l.matchesplayed, l.kills, l.minutesplayed, l.top25, l.top10, l.top1, l.date
      FROM st__live l
      INNER JOIN st__all_platforms ap ON l.platform = ap.id
      INNER JOIN st__all_modes am ON l.mode = am.id
      WHERE l.player_id = ?
      ORDER BY l.date DESC
      LIMIT 8`;
    const [liveStats] = await this.db.execute(sql, [this.playerId]);
    return liveStats;
  }

  async history() {
    const sql = `SELECT player_id, SUM(score) score, SUM(fgscore) fgscore, SUM(matchesplayed) matchesplayed,  SUM(kills) kills, SUM(minutesplayed) minutesplayed, SUM(top25) top25, SUM(top10) top10, SUM(top1) top1, DATE(date) datedOn 
      FROM st__live
      WHERE player_id = ?
      GROUP BY datedOn
      ORDER BY datedOn DESC
      LIMIT 7`;
    const [statsHistory] = await this.db.execute(sql, [this.playerId]);
    return statsHistory;
  }

  general(allStats, name) {
    const general = allStats
      .reduce((general, stats) => {
        for (const prop in general) {
          general[prop] += parseFloat(stats[prop]);
        }
        return general;
      }, { matchesplayed: 0, kills: 0, minutesplayed: 0, top1: 0, winratio: 0, kdratio: 0 });
    
    const roundNum = number => Math.round(number * 100) / 100;

    return {
      name,
      ...general,
      winratio: roundNum(general.winratio / allStats.length),
      kdratio: roundNum(general.kdratio / allStats.length)
    }
  }
}

module.exports = GetStatsDb;