const express = require('express');
const app = express();
const cors = require('cors');
const morgan = require('morgan');
const mysql = require('mysql2/promise');
// const winston = require('./config/winston');

const NODE_ENV = process.env.NODE_ENV;

app.use(cors());
app.use(morgan(NODE_ENV === 'development' ? 'dev' : ('combined', { stream: winston.stream })));

app.use('/static', express.static('static'));

app.use(async (req, res, next) => {
  res.db = await mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    multipleStatements: true
  });
  next();
});

const itemsRoutes = require('./api/routes/items');
const statsRoutes = require('./api/routes/stats');

app.use('/items', itemsRoutes);
app.use('/stats', statsRoutes);

app.use((req, res, next) => res.status(404).json({ msg: `Inappropriate request` }));

app.use((err, req, res, next) => {
  // console.log(NODE_ENV)
  throw (err);
  // winston.error(`${err.status || 500} - ${err.message || err.msg} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
  res.status(err.status || 500); 
  res.json({
    msg: NODE_ENV === 'development' ? err.message || err.msg || 'Unknown error' : 'Internal server error'
  });
});

app.listen(4000);